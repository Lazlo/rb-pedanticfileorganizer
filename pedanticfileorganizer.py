#!/usr/bin/env python3

import gi

gi.require_version('RB', '3.0')

from gi.repository import GObject, Peas
from gi.repository import RB

class PedanticFileOrganizer(GObject.Object, Peas.Activatable):

    __gtype_name = 'pedanticfileorganizer'
    object = GObject.property(type=GObject.Object)

    def __init__(self, *args, **kwargs):
        GObject.Object.__init__(self)
        super(PedanticFileOrganizer, self).__init__(*args, **kwargs)

        self.shell = None
        self.rbdb = None

    # Rhythmbox standard Activate method
    def do_activate(self):
        shell = self.object
        self.shell = shell
        self.rbdb = shell.props.db

    # Rhythmbox standard Deactivate method
    def do_deactivate(self):
        return

class PythonSource(RB.Source):
    """ Register with Rhythmbox """

    def __init__(self):
        RB.source.__init__(self)
        GObject.type_register_dynamic(PythonSource)
