PLUGIN_NAME=pedanticfileorganizer
INSTALL_DIR=$(HOME)/.local/share/rhythmbox/plugins/$(PLUGIN_NAME)

install:
	mkdir -p $(INSTALL_DIR)
	cp $(PLUGIN_NAME).plugin $(INSTALL_DIR)
	cp $(PLUGIN_NAME).py $(INSTALL_DIR)
